package AIF.AerialVehicles.Exceptions;

public class ModuleAlreadyUsedException extends AVException {
    public ModuleAlreadyUsedException() { super("Module already been used."); }
}
