package AIF.AerialVehicles.uav.haron;

import AIF.AerialVehicles.modules.AttackModule;
import AIF.AerialVehicles.modules.IntelligenceSensor;
import AIF.Entities.Coordinates;

public class Eitan extends Haron {
    private static final Class[] compatibleModuleTypes = {AttackModule.class, IntelligenceSensor.class};
    private static final int moduleStations = 4;

    public Eitan(Coordinates coordinates) {
        super(coordinates, compatibleModuleTypes, moduleStations);
    }
}
