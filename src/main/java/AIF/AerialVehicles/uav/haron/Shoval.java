package AIF.AerialVehicles.uav.haron;

import AIF.AerialVehicles.modules.AttackModule;
import AIF.AerialVehicles.modules.BDAModule;
import AIF.AerialVehicles.modules.IntelligenceSensor;
import AIF.Entities.Coordinates;

public class Shoval extends Haron {
    private static final Class[] compatibleModuleTypes = {AttackModule.class, IntelligenceSensor.class, BDAModule.class};
    private static final int moduleStations = 3;

    public Shoval(Coordinates coordinates) {
        super(coordinates, compatibleModuleTypes, moduleStations);
    }
}

