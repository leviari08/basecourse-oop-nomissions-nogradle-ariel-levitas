package AIF.AerialVehicles.uav.haron;

import AIF.AerialVehicles.uav.UAV;
import AIF.Entities.Coordinates;

public abstract class Haron extends UAV {
    private static final int HARON_FLYING_LIMIT = 15000;

    public Haron(Coordinates coordinates, Class[] compatibleModuleTypes, int moduleStations) {
        super(coordinates, HARON_FLYING_LIMIT, compatibleModuleTypes, moduleStations);
    }
}
