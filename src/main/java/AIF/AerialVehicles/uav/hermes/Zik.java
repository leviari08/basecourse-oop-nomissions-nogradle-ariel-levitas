package AIF.AerialVehicles.uav.hermes;

import AIF.AerialVehicles.modules.BDAModule;
import AIF.AerialVehicles.modules.IntelligenceSensor;
import AIF.Entities.Coordinates;

public class Zik extends Hermes {
    private static final Class[] compatibleModuleTypes = {IntelligenceSensor.class, BDAModule.class};
    private static final int moduleStations = 1;

    public Zik(Coordinates coordinates) {
        super(coordinates, compatibleModuleTypes, moduleStations);
    }
}