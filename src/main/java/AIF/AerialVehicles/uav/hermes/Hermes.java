package AIF.AerialVehicles.uav.hermes;

import AIF.AerialVehicles.uav.UAV;
import AIF.Entities.Coordinates;

public abstract class Hermes extends UAV {
    private static final int HERMES_FLYING_LIMIT = 10000;

    public Hermes(Coordinates coordinates, Class[] compatibleModuleTypes, int moduleStations) {
        super(coordinates, HERMES_FLYING_LIMIT, compatibleModuleTypes, moduleStations);
    }
}
