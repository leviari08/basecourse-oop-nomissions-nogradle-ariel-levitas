package AIF.AerialVehicles.uav.hermes;

import AIF.AerialVehicles.modules.AttackModule;
import AIF.AerialVehicles.modules.BDAModule;
import AIF.AerialVehicles.modules.IntelligenceSensor;
import AIF.Entities.Coordinates;

public class Kochav extends Hermes {
    private static final Class[] compatibleModuleTypes = {AttackModule.class, IntelligenceSensor.class, BDAModule.class};
    private static final int moduleStations = 5;

    public Kochav(Coordinates coordinates) {
        super(coordinates, compatibleModuleTypes, moduleStations);
    }
}
