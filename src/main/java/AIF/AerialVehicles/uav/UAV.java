package AIF.AerialVehicles.uav;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.Exceptions.CannotPerformOnGroundException;
import AIF.Entities.Coordinates;

public abstract class UAV extends AerialVehicle {
    private final int avgSpeed = 150;

    public UAV(Coordinates coordinates, int flyingDistanceBeforeMaintenance, Class[] compatibleModuleTypes, int moduleStations) {
        super(coordinates, flyingDistanceBeforeMaintenance, compatibleModuleTypes, moduleStations);
    }

    public void hoverOverLocation(int hours) throws CannotPerformOnGroundException {
        if (!this.isInAir) {
            throw new CannotPerformOnGroundException();
        }

        System.out.println("Hovering over: " + this.coordinates);
        this.flownDistanceSinceLastMaintenance += hours * avgSpeed;
    }
}
