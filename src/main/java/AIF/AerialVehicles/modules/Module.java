package AIF.AerialVehicles.modules;

import AIF.AerialVehicles.Exceptions.ModuleAlreadyUsedException;
import AIF.AerialVehicles.Exceptions.NoModuleCanPerformException;
import AIF.Entities.Coordinates;

public abstract class Module {
    private final int range;

    public Module(int range) {
        this.range = range;
    }

    public void activateModule(Coordinates aircraftCoordinates, Coordinates targetCoordinates) throws ModuleAlreadyUsedException, NoModuleCanPerformException {
        if (aircraftCoordinates.distance(targetCoordinates) > range) {
            throw new NoModuleCanPerformException();
        }
    }
}
