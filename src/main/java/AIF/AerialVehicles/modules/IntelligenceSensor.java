package AIF.AerialVehicles.modules;

import AIF.AerialVehicles.Exceptions.ModuleAlreadyUsedException;
import AIF.AerialVehicles.Exceptions.NoModuleCanPerformException;
import AIF.Entities.Coordinates;

public class IntelligenceSensor extends Module {
    private static final int RANGE = 600;

    public IntelligenceSensor() {
        super(RANGE);
    }

    @Override
    public void activateModule(Coordinates aircraftCoordinates, Coordinates targetCoordinates) throws ModuleAlreadyUsedException, NoModuleCanPerformException {
        super.activateModule(aircraftCoordinates, targetCoordinates);
        // intelligence stuff in coordinates
        System.out.println("TODO: ask 8200 to say what they are doing here");
    }

    @Override
    public String toString() {
        return "Intelligence module";
    }
}
