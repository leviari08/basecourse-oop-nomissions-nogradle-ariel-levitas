package AIF.AerialVehicles.modules;

import AIF.AerialVehicles.Exceptions.ModuleAlreadyUsedException;
import AIF.AerialVehicles.Exceptions.NoModuleCanPerformException;
import AIF.Entities.Coordinates;

public class BDAModule extends Module {
    private static final int RANGE = 300;

    public BDAModule() {
        super(RANGE);
    }

    @Override
    public void activateModule(Coordinates aircraftCoordinates, Coordinates targetCoordinates) throws ModuleAlreadyUsedException, NoModuleCanPerformException {
        super.activateModule(aircraftCoordinates, targetCoordinates);
        // BDA in coordinates
        System.out.println("Doing BDA stuff");
    }

    @Override
    public String toString() {
        return "BDA Module";
    }
}
