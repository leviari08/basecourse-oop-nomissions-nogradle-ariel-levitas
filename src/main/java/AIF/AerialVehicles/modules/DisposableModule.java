package AIF.AerialVehicles.modules;

import AIF.AerialVehicles.Exceptions.ModuleAlreadyUsedException;
import AIF.AerialVehicles.Exceptions.NoModuleCanPerformException;
import AIF.Entities.Coordinates;

public abstract class DisposableModule extends Module {
    private boolean beenUsed = false;

    public DisposableModule(int range) {
        super(range);
    }

    public boolean isBeenUsed() {
        return beenUsed;
    }

    @Override
    public void activateModule(Coordinates aircraftCoordinates, Coordinates targetCoordinates) throws ModuleAlreadyUsedException, NoModuleCanPerformException {
        super.activateModule(aircraftCoordinates, targetCoordinates);

        if (this.beenUsed) {
            throw new ModuleAlreadyUsedException();
        }

        beenUsed = true;
    }
}
