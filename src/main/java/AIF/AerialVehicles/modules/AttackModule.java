package AIF.AerialVehicles.modules;

import AIF.AerialVehicles.Exceptions.ModuleAlreadyUsedException;
import AIF.AerialVehicles.Exceptions.NoModuleCanPerformException;
import AIF.Entities.Coordinates;

public class AttackModule extends DisposableModule {
    private static final int RANGE = 1500;

    public AttackModule() {
        super(RANGE);
    }

    @Override
    public void activateModule(Coordinates aircraftCoordinates, Coordinates targetCoordinates) throws ModuleAlreadyUsedException, NoModuleCanPerformException {
        super.activateModule(aircraftCoordinates, targetCoordinates);

        // Attack in coordinates
        System.out.println("pew pew!");
    }

    @Override
    public String toString() {
        return "Attack Module";
    }
}
