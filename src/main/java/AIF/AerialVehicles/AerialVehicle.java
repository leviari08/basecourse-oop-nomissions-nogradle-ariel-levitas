package AIF.AerialVehicles;


import AIF.AerialVehicles.Exceptions.*;
import AIF.AerialVehicles.modules.DisposableModule;
import AIF.AerialVehicles.modules.Module;
import AIF.Entities.Coordinates;

import java.util.Arrays;
import java.util.Optional;

public abstract class AerialVehicle {
    protected boolean isInAir = false;
    protected Coordinates coordinates;
    private final int flyingLimit;
    protected int flownDistanceSinceLastMaintenance = 0;
    private final Class[] compatibleModuleTypes;
    private final int moduleStations;
    private final Module[] modules;

    public AerialVehicle(Coordinates coordinates, int flyingLimit, Class[] compatibleModuleTypes, int moduleStations) {
        this.coordinates = coordinates;
        this.flyingLimit = flyingLimit;
        this.compatibleModuleTypes = compatibleModuleTypes;
        this.moduleStations = moduleStations;
        this.modules = new Module[moduleStations];
    }

    public void takeOff() throws CannotPerformInMidAirException, NeedMaintenanceException {
        if (this.isInAir) {
            throw new CannotPerformInMidAirException();
        } else if (this.needMaintenance()) {
            throw new NeedMaintenanceException();
        } else {
            System.out.println("Taking off");
            this.isInAir = true;
        }
    }

    public void flyTo(Coordinates destination) throws CannotPerformOnGroundException {
        if (!this.isInAir) {
            throw new CannotPerformOnGroundException();
        }

        System.out.println("Flying to: " + destination);
        double distanceFlew = this.coordinates.distance(destination);
        this.flownDistanceSinceLastMaintenance += distanceFlew;
        this.coordinates = destination;
    }

    public void land() throws CannotPerformInMidAirException {
        if (!this.isInAir) {
            throw new CannotPerformInMidAirException();
        }

        System.out.println("Landing");
        this.isInAir = false;
    }

    public boolean needMaintenance() {
        return this.flownDistanceSinceLastMaintenance >= flyingLimit;
    }

    public void performMaintenance() throws CannotPerformInMidAirException {
        if (this.isInAir) {
            throw new CannotPerformInMidAirException();
        }

        System.out.println("Performing maintenance");
        this.flownDistanceSinceLastMaintenance = 0;
    }

    public void loadModule(Module module) throws NoModuleStationAvailableException, ModuleNotCompatibleException, CannotPerformInMidAirException {
        if (isInAir) {
            throw new CannotPerformInMidAirException();
        } else if (countModules() == moduleStations) {
            throw new NoModuleStationAvailableException();
        } else if (!isModuleCompatible(module)) {
            throw new ModuleNotCompatibleException();
        }

        for (int i = 0; i < modules.length; i++) {
            if (modules[i] == null) {
                modules[i] = module;
                System.out.println(module + " loaded");
                break;
            }
        }
    }

    public void activateModule(Class<? extends Module> moduleClass, Coordinates targetCoordinates) throws ModuleNotFoundException, CannotPerformOnGroundException, NoModuleCanPerformException, ModuleAlreadyUsedException {
        Optional<Module> moduleFromType = Arrays.stream(modules).filter(module -> module.getClass() == moduleClass).findAny();

        if (moduleFromType.isEmpty()) {
            throw new ModuleNotFoundException();
        } else if (!this.isInAir) {
            throw new CannotPerformOnGroundException();
        }

        // check if module is disposable
        if (DisposableModule.class.isAssignableFrom(moduleClass)) {
            // module is disposable
            Optional<Module> nonUsedModuleFromType = Arrays.stream(modules).filter(module -> module.getClass() == moduleClass && !((DisposableModule) module).isBeenUsed()).findAny();

            if (nonUsedModuleFromType.isEmpty()) {
                throw new NoModuleCanPerformException();
            }

            nonUsedModuleFromType.get().activateModule(coordinates, targetCoordinates);
        } else {
            // module is not disposable
            moduleFromType.get().activateModule(coordinates, targetCoordinates);
        }

        System.out.printf("Activate module %s at %s%n", moduleFromType.get(), targetCoordinates);
    }

    private int countModules() {
        long nonNullModules = Arrays.stream(modules).filter(module -> module != null).count();
        return (int) nonNullModules;
    }

    private boolean isModuleCompatible(Module module) {
        return Arrays.stream(compatibleModuleTypes).anyMatch(m -> m.getName().equals(module.getClass().getName()));
    }
}
