package AIF.AerialVehicles.figherAircraft;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Entities.Coordinates;

public abstract class FighterAircraft extends AerialVehicle {
    private static final int FLYING_LIMIT = 25000;

    public FighterAircraft(Coordinates coordinates, Class[] compatibleModuleTypes, int moduleStations) {
        super(coordinates, FLYING_LIMIT, compatibleModuleTypes, moduleStations);
    }
}
