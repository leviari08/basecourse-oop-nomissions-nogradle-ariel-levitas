package AIF.AerialVehicles.figherAircraft;

import AIF.AerialVehicles.modules.AttackModule;
import AIF.AerialVehicles.modules.BDAModule;
import AIF.Entities.Coordinates;

public class F16 extends FighterAircraft {
    private static final Class[] compatibleModuleTypes = {AttackModule.class, BDAModule.class};
    private static final int moduleStations = 7;

    public F16(Coordinates coordinates) {
        super(coordinates, compatibleModuleTypes, moduleStations);
    }
}
