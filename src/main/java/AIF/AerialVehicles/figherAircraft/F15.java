package AIF.AerialVehicles.figherAircraft;

import AIF.AerialVehicles.modules.AttackModule;
import AIF.AerialVehicles.modules.IntelligenceSensor;
import AIF.Entities.Coordinates;

public class F15 extends FighterAircraft {
    private static final Class[] compatibleModuleTypes = {AttackModule.class, IntelligenceSensor.class};
    private static final int moduleStations = 10;

    public F15(Coordinates coordinates) {
        super(coordinates, compatibleModuleTypes, moduleStations);
    }
}
